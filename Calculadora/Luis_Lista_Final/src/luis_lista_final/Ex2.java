package luis_lista_final;

import java.util.Scanner;

public class Ex2 {

    public static void main(String[] args) {

        Scanner ler = new Scanner(System.in);
        int c = 0;
        double t = 0;
        double[] n = new double[5];

        for (int i = 0; i < n.length; i++) {

            n[i] = ler.nextDouble();

            if (n[i] >= 0) {

                t = t + n[i];

            } else {

                c++;

            }

        }

        System.out.println(c + " numeros negativos e o total dos positivos é " + t);

    }

}
