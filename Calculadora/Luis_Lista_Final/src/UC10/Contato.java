package UC10;

import com.sun.java.accessibility.util.SwingEventMonitor;
import java.awt.AWTEventMulticaster;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class Contato {

    public static class Contatos {

        String nome;
        String telefone;
    }

    public static void main(String[] args) {

        Scanner ler = new Scanner(System.in);
        int op;
        String n;
        String menu;
        String l = null;
        int a = 1;
        List<Contatos> contatos = new ArrayList<>();

        do {
            menu = JOptionPane.showInputDialog("Agenda \n"
                    + "Contatos (" + contatos.size() + ")\n"
                    + "1 - Novo Contato\n"
                    + "2 - Buscar Contato por Nome\n"
                    + "3 - Remover Contato\n"
                    + "4 - Lista de Contatos\n"
                    + "5 - Sobre\n"
                    + "0 - Sair\n");

            op = Integer.parseInt(menu);

            switch (op) {

                case 1:
                    a = 1;
                    Contatos contato = new Contatos();

                    menu = JOptionPane.showInputDialog("Digite o nome do novo contato");

                    contato.nome = menu;

                    n = menu;

                    if (!contatos.isEmpty()) {

                        for (int i = 0; i < contatos.size(); i++) {

                            if (n.equalsIgnoreCase(contatos.get(i).nome)) {

                                JOptionPane.showMessageDialog(null, "O nome " + contatos.get(i).nome + " já foi cadastrado");
                                a = 4;

                            }
                        }

                    }
                    if (a == 4) {
                        break;
                    }

                    menu = JOptionPane.showInputDialog("Digite o numero de " + contato.nome);
                    contato.telefone = menu;
                    n = menu;

                    if (!contatos.isEmpty()) {

                        for (int i = 0; i < contatos.size(); i++) {

                            if (n.equalsIgnoreCase(contatos.get(i).telefone)) {

                                JOptionPane.showMessageDialog(null, "O telefone " + contatos.get(i).telefone + " já foi cadastrado");
                                a = 4;

                            }
                        }

                    }
                    if (a == 4) {
                        break;
                    }

                    contatos.add(contato);

                    JOptionPane.showMessageDialog(null, "Contato Salvo com Sucesso!!!");

                    break;
                case 2:
                    if (contatos.isEmpty()) {

                        JOptionPane.showMessageDialog(null, "A lista está vazia!!!");

                        break;
                    }
                    menu = JOptionPane.showInputDialog("Busque o contato por nome");
                    n = menu;

                    for (int i = 0; i < contatos.size(); i++) {

                        if (n.equalsIgnoreCase(contatos.get(i).nome)) {
                            JOptionPane.showMessageDialog(null, "O nome " + contatos.get(i).nome
                                    + " está cadastrado\n" + "seu numero é " + contatos.get(i).telefone);
                            a = 1;
                            break;
                        }
                    }
                    if (a != 1) {
                        JOptionPane.showMessageDialog(null, "O contato não existe");

                    }
                    break;
                case 3:
                    if (contatos.isEmpty()) {

                        JOptionPane.showMessageDialog(null, "A lista está vazia!!!");

                        break;
                    }
                    menu = JOptionPane.showInputDialog("Digite o nome a apagar da agenda");
                    n = menu;

                    for (int i = 0; i < contatos.size(); i++) {

                        if (n.equalsIgnoreCase(contatos.get(i).nome)) {
                            contatos.remove(i);
                            JOptionPane.showMessageDialog(null, "Contato " + n + "removido com sucesso!!!");
                            a = 2;

                            break;

                        }

                    }

                    if (a != 2) {

                        JOptionPane.showMessageDialog(null, "O contato não existe ");

                    }
                    break;
                case 4:
                    if (contatos.isEmpty()) {

                        JOptionPane.showMessageDialog(null, "A lista está vazia!!!");

                        break;
                    }

                    for (int i = 0; i < contatos.size(); i++) {

                        l += "Nome:" + contatos.get(i).nome + "\n" + "Telefone: " + contatos.get(i).telefone + "\n\n";

                    }

                    JOptionPane.showMessageDialog(null, l);
                    break;

                case 5:
                    JOptionPane.showMessageDialog(null, "Aplicativo feito por\n"
                            + "Luis G. L. Fernandes\n" + "2019");
                    break;

                case 0:

                    System.exit(0);
                default:
                    JOptionPane.showMessageDialog(null, "Operação inexistente!!!");
                    break;
            }

        } while (true);
    }

}
